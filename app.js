const request = require("request");
const process = require("process");
const dotenv = require("dotenv");
const WebSocket = require("ws");
const grpc = require("@grpc/grpc-js");
const protoLoader = require("@grpc/proto-loader");

dotenv.config();

const ema = require("./strategies/ema.js");

const streamUrl = "wss://stream.binance.com:9443";
const orderExecuterUrl = "localhost:50051";
const packageDefinition = protoLoader.loadSync("./trade.proto", {
    keepcase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true,
});
const NOTIFY_URL = "https://notify-api.line.me/api/notify";

const protoDescriptor = grpc.loadPackageDefinition(packageDefinition);
const executorProto = protoDescriptor.Executor;
const executor = new executorProto(
    orderExecuterUrl,
    grpc.credentials.createInsecure(),
);

const tradingCoins = ["dotusdt"];
const streams = tradingCoins.map((e) => e + "@kline_1h").join("/");

/*
https://api1.binance.com
https://api2.binance.com
https://api3.binance.com
*/

const algs = {
    ema,
};

const init = async (algName) => {
    await algs[algName].initialize();
};

const getData = async (algName) => {
    const ws = new WebSocket(streamUrl + "/stream?streams=" + streams);

    ws.on("open", function open() {
        sendNotification("Connected to binance");
    });

    ws.on("message", function incoming(stream) {
        const data = JSON.parse(stream).data;
        const ticker = data.s;
        const closePrice = data.k.c;

        if (!data.k.x) return; //if kline is not closed`

        algs[algName].update(ticker, closePrice);
        const shouldEnter = algs[algName].shouldEnter(ticker);
        const shouldSell = algs[algName].shouldSell(ticker);

        if (shouldEnter) {
            sendNotification("Buying " + ticker);
            executor.buy({ ticker }, function(err, res) {
                console.log(res.messages);
            });
            return;
        }

        if (shouldSell) {
            sendNotification("Selling " + ticker);
            executor.sell({ ticker }, function(err, res) {
                console.log(res.messages);
            });
            return;
        }
    });

    ws.on("error", (error) => {
        sendNotification("Error" + error.message);
    });

    ws.on("close", function close() {
        sendNotification("Disconnected from binace\nReconnecting...");

        setTimeout(function() {
            getData(algName);
        }, 1000);
    });
};

const main = async (algName) => {
    await init(algName);
    getData(algName);
};

main(process.argv[2] || "ema");

process.once("SIGTERM", gracefuleShutdown);
process.once("SIGINT", gracefuleShutdown);

function gracefuleShutdown() {
    sendNotification("Shutting down...");
    setTimeout(() => {
        process.exit();
    }, 1000);
}

function sendNotification(message) {
    request(
        {
            method: "POST",
            uri: NOTIFY_URL,
            header: {
                "Content-Type": "multiplart/form-data",
            },
            auth: {
                bearer: process.env.TOKEN,
            },
            form: {
                message: message,
            },
        },
        (err, _, body) => {
            if (err) {
                console.log(err);
            } else {
                console.log(body);
            }
        },
    );
}
