// 1h timeframe
const fs = require("fs");
const path = require("path");
const axios = require("axios");
const baseUrl = "https://api.binance.com";
const starter = require("./ema.json");
var coins = {};

function calculateEma(prevEma, value, days) {
  const multiplier = 2 / (1 + days);
  const result = value * multiplier + prevEma * (1 - multiplier);
  // return result;
  return Math.round((result + Number.EPSILON) * 1000000) / 1000000;
}

async function initialize() {
  coins = starter.coins;
  const startTime = starter.timestamp + 3600000;
  const resServerTime = await axios.get(baseUrl + "/api/v3/time");
  const endTime = resServerTime.data.serverTime - 3600000; //prev 1h candle stick
  let lastStickTime = starter.timestamp;

  for (const [key, value] of Object.entries(coins)) {
    const res = await axios.get(
      baseUrl +
        `/api/v3/klines?symbol=${key.toUpperCase()}&interval=1h&startTime=${startTime}&endTime=${endTime}`,
    );
    const sticks = res.data;
    sticks.forEach((stick) => {
      update(key, stick[4]);
      lastStickTime = stick[0];
    });
  }

  writeToFile(lastStickTime);
}

function update(ticker, closePrice, klineStartTime) {
  const coin = ticker.toLowerCase();
  coins[coin].prevEma12 = coins[coin].ema12;
  coins[coin].prevEma26 = coins[coin].ema26;

  coins[coin].ema12 = calculateEma(coins[coin].ema12, closePrice, 12);
  coins[coin].ema26 = calculateEma(coins[coin].ema26, closePrice, 26);

  writeToFile(klineStartTime);
}

function shouldEnter(ticker) {
  const coin = coins[ticker.toLowerCase()];

  return coin.prevEma12 <= coin.prevEma26 && coin.ema12 > coin.ema26;
}

function shouldSell(ticker) {
  const coin = coins[ticker.toLowerCase()];

  return coin.prevEma12 >= coin.prevEma26 && coin.ema12 < coin.ema26;
}

function writeToFile(timestamp) {
  fs.writeFileSync(
    path.join(__dirname, "/ema.json"),
    JSON.stringify(
      {
        coins: coins,
        timestamp: timestamp,
        test: "tset",
      },
      null,
      "\t",
    ),
  );
}

module.exports = {
  coins,
  initialize,
  shouldEnter,
  update,
  shouldSell,
};
